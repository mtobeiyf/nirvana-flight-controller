#include "center_task.h"

//初始化任务
u8 Cur_Task = t_Manual_Control;

u8 Pre_Task = t_Manual_Control;

u8 Task_Finish_Flag = 0;

u8 Rc_Func = t_Manual_Control;

u8 Landing_Flag = 0;

// float FLY_ANGLE_F = 0.30;
// float FLY_ANGLE_B = 3.40;
// float FLY_ANGLE_L = 0.30;
// float FLY_ANGLE_R = 0.40;
// float FLY_ANGLE_R2 = 0.40;
float FLY_ANGLE_F = 1.0;
float FLY_ANGLE_B = 1.0;
float FLY_ANGLE_L = 0.01;
float FLY_ANGLE_R = 0.30;
float FLY_ANGLE_R2 = 1.30;
float x_offset = 0;
float y_offset = 0;

float brake_speed = 2.0;

u16 brake_time = 250;
// u16 brake_time_F = 100;
// u16 brake_time_B = 150;
// u16 brake_time_L = 150;
// u16 brake_time_R = 20;
u16 brake_time_F = 0;
u16 brake_time_B = 0;
u16 brake_time_L = 0;
u16 brake_time_R = 0;

float set_speed_x = 0.0f;
float set_speed_y = 0.0f;
u8 use_of_2 = 0; //默认不开启串级光流数据

//更新当前任务
void Get_Task(void)
{
    height_ctrl_mode = 2; //默认超声波定高

    //遥控功能
    switch (Rc_Func)
    {
    case 0:
        Cur_Task = t_Urgent_Stop;
        break;
    case 21:
        //起飞悬停
        Cur_Task = t_Fly_Hover;
        break;
    case 22:
        //降落
        Cur_Task = t_Throw;
        break;
    case 23:
        //捡铁片
        Cur_Task = t_Pick;
        break;
    case 24:
        //丢铁片
        Cur_Task = t_Fly_Rectangle;
        break;

    case 31:
        //前
        Cur_Task = t_Fly_Forward;
        break;
    case 32:
        //后
        Cur_Task = t_Fly_Backward;
        break;
    case 33:
        //左
        Cur_Task = t_Fly_Left;
        break;
    case 34:
        //右
        Cur_Task = t_Fly_Right;
        break;

    case 41:
        //A-B
        Cur_Task = t_Fly_A_To_B;
        break;
    case 42:
        //矩形
        Cur_Task = t_Fly_Rectangle;
        break;
    case 43:
        //A到B丢铁片
        Cur_Task = t_Line_Hover;
        break;
    case 44:
        //A到B捡铁片
        Cur_Task = t_Fly_Hover;
        break;
    default:
        Cur_Task = t_Manual_Control;
        break;
    }

    if (Rc_Pwm_In[7] > 1700)
    {
        Cur_Task = t_Manual_Control;
        if (Rc_Pwm_In[4] > 1700)
        {
            //下
            Cur_Task = t_Fly_Hover;
        }
        else if (Rc_Pwm_In[4] < 1300)
        {
            //上
            Cur_Task = t_Manual_Control;
        }
        else if (Rc_Pwm_In[4] > 1400 && Rc_Pwm_In[4] < 1600)
        {
            Cur_Task = t_Line_Hover;
        }
    }

    if (Task_Finish_Flag)
        Cur_Task = t_Manual_Control; //切换回手动模式
    //自动降落
    if (Rc_Pwm_In[6] > 1700)
    {
        Cur_Task = t_Landing;
        Task_Finish_Flag = 0;
    }

    //急停功能
    if (Rc_Pwm_In[5] > 1700 || NS == 0)
    {
        Cur_Task = t_Urgent_Stop;
        Task_Finish_Flag = 0;
    }

    //使用自己的油门变量
    for (u8 i = 0; i < 4; ++i)
    {
        My_Rc_Pwm_In[i] = Rc_Pwm_In[i];
    }
}

//执行任务
void Run_Task(void)
{
    switch (Cur_Task)
    {
    case t_Urgent_Stop:
        Task_Urgent_Stop(); //急停任务
        break;
    case t_Landing:
        Task_Landing(); //降落任务
        break;
    case t_Manual_Control:
        Task_Manual_Control(); //手动模式
        break;
    case t_Line_Hover:
        Task_Line_Hover(); //线上悬停
        break;
    case t_Fly_A_To_B:
        Task_Fly_A_To_B(); //A飞到B
        break;
    case t_Fly_Rectangle:
        Task_Fly_Rectangle(); //飞矩形
        break;
    case t_Throw_Metal:
        Task_Throw_Metal(); //投铁片
        break;
    case t_Line_Hover_Y:
        Task_Line_Hover_Y(); //横向悬停
        break;
    case t_Object_Hover:
        Task_Object_Hover(); //定点悬停
        break;
    case t_Object_Track:
        Task_Object_Track(); //定点悬停
        break;
    case t_Rectangle_Yaw:
        Task_Rectangle_Yaw(); //矩形
        break;
    case t_Fly_Hover:
        Task_Fly_Hover(); //起飞悬停
        break;
    case t_Fly_Forward:
        Task_Fly_Forward();
        break;
    case t_Fly_Backward:
        Task_Fly_Backward();
        break;
    case t_Fly_Left:
        Task_Fly_Left();
        break;
    case t_Fly_Right:
        Task_Fly_Right();
        break;
    default:
        Task_Manual_Control(); //默认用遥控输入的参数
        break;
    }
    Yaw_Control();
}

//获取并执行任务
void My_Task(void)
{
    RC_Rx_Anl(rc_rxbuf);
    Get_Task();
    Run_Task();

    signal_loss++;
    if (signal_loss > 20)
    {
        signal_loss = 0;
        signal_strength = 0;
    }
}

//急停任务
void Task_Urgent_Stop(void)
{
    My_Rc_Pwm_In[2] = 1100;
    fly_ready = 0;
    Height_Control(ULTRA_HOLD_HEIGHT);
}

//降落任务
void Task_Landing(void)
{
    // //旧版降落
    // if (fly_ready == 1)
    // {
    //     if (ultra_distance > 80 && fly_ready == 1)
    //     {
    //         My_Rc_Pwm_In[2] = 1380;
    //     }
    //     else
    //     {
    //         My_Rc_Pwm_In[2] = 1100;
    //         fly_ready = 0;
    //     }
    // }
    if (fly_ready == 1)
    {
        Landing_Flag = 1;
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(0);
        if (ultra_distance <= 120)
        {
            fly_ready = 0;
            Height_Control(ULTRA_HOLD_HEIGHT);
            Landing_Flag = 0;
        }
    }
}

//手动模式输出
void Task_Manual_Control(void)
{
    // if (!fly_ready && NS != 0)
    // {
    //     fly_ready = 1; //解锁
    // }
    except_A.x = MAX_CTRL_ANGLE * (my_deathzoom((CH_filter[ROL]), 30) / 500.0f);  //30
    except_A.y = MAX_CTRL_ANGLE * (my_deathzoom((-CH_filter[PIT]), 30) / 500.0f); //30
}

//悬停在线上
void Task_Line_Hover(void)
{
    if (!fly_ready)
    {
        ultra_hold_height = ULTRA_HOLD_HEIGHT;
        fly_ready = 1; //解锁
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ultra_hold_height);
    if (ultra_distance <= CONTROL_HEIGHT && NS != 0)
    {
        //未达到高度，期望水平为0
        except_A.x = 0;
        except_A.y = 0;
    }
    else
    {
        //角度进行限制
        except_A.x = angle_exp.x;
        //前后用油门校偏差
        except_A.y = MAX_CTRL_ANGLE * (my_deathzoom((-CH_filter[PIT]), 30) / 500.0f); //30
        // except_A.y = of_exp_1.y;
    }
}

//由A飞到B
void Task_Fly_A_To_B()
{
    static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
        set_speed_x = 0.0f;
        set_speed_y = 0.0f;
    }
    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }
    if (ultra_distance > CONTROL_HEIGHT && !t_flag)
    {
        //向前倾
        except_A.y = -FLY_ANGLE_F;
        // set_speed_y = -FLY_ANGLE_F;
        // except_A.y = of_exp_1.y;
        except_A.x = angle_exp.x;
        of_xy_1[0].err_i.y = 0.0f;

        if (camera_mode == CAM_TP)
        {
            t_flag = 1;
            t_cnt = 0;
            set_speed_y = 0.0f;
            except_A.y = 0;
            except_A.x = 0;
        }
    }
    if (t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR;
        t_cnt++;
        if (t_cnt < brake_time_F)
        {
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y + brake_speed;
        }
        else if (t_cnt < brake_time_F + 800)
        {
            Height_Control(200);
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
        }
        else
        {
            t_cnt = 10000;
            Task_Landing();
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            if (!fly_ready)
            {
                t_flag = 0;
                t_cnt = 0;
                Task_Finish_Flag = 1;
            }
        }
    }
}

//投掷
float FLY_METAL_F = 0.40;
float FLY_METAL_B = 3.20;
u16 brake_metal_F = 300;
u16 brake_metal_B = 100;
//投掷铁片
void Task_Throw_Metal(void)
{
    static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落
    static u8 dir = 0;    //方向

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
        dir = 0;
        EMPlug_Set(High);
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (ultra_distance > CONTROL_HEIGHT && !t_flag)
    {
        switch (dir)
        {
        case 0:
            //向前倾
            except_A.y = -FLY_METAL_F;
            except_A.x = angle_exp.x;

            if (camera_mode == CAM_TP)
            {
                dir = 1;
                t_cnt = 0;
                except_A.y = angle_exp.y;
                except_A.x = angle_exp.x;
            }
            break;
        case 1:
            t_cnt++;
            if (t_cnt < brake_metal_F + 400)
            {
                if (t_cnt < brake_metal_F)
                {
                    //刹车
                    except_A.x = angle_exp.x;
                    except_A.y = angle_exp.y + brake_speed;
                }
                else
                {
                    //悬停
                    except_A.x = angle_exp.x;
                    except_A.y = angle_exp.y;
                }
            }
            else
            {
                EMPlug_Set(Low); //投铁片
                dir = 2;
                t_cnt = 0;
            }
            break;
        case 2:
            //倒着回去
            except_A.y = FLY_METAL_B;
            except_A.x = angle_exp.x;

            if (camera_mode == CAM_TN)
            {
                t_flag = 1;
                except_A.y = angle_exp.y;
                except_A.x = angle_exp.x;
            }
            break;
        }
    }
    if (t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR;
        t_cnt++;
        if (t_cnt < brake_metal_B + 400)
        {
            if (t_cnt < brake_metal_B)
            {
                //刹车
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y - brake_speed;
            }
            else
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
        }
        else
        {
            t_cnt = 10000;
            Task_Landing();
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            if (!fly_ready)
            {
                t_flag = 0;
                t_cnt = 0;
                Task_Finish_Flag = 1;
            }
        }
    }
}

void Task_Pick_Metal(void)
{
    static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落
    static u8 dir = 0;    //方向
    static u16 t_cnt2 = 0;
    u8 t_height = 80;

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
        dir = 0;
        EMPlug_Set(Low);
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (!t_flag)
    {
        switch (dir)
        {
        case 0:
            //向前倾
            except_A.y = -FLY_ANGLE_F;
            except_A.x = angle_exp.x;

            if (camera_mode == CAM_TP)
            {
                dir = 1;
                t_cnt = 0;
                except_A.y = angle_exp.y;
                except_A.x = angle_exp.x;
            }
            break;
        case 1:
            t_cnt++;
            if (t_cnt < 800)
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            else
            {
                Height_Control(t_height);
                if (ultra_hold_height == t_height)
                {
                    dir = 2;
                    t_cnt = 0;
                }
            }
            break;
        case 2:
            t_cnt++;
            if (t_cnt < 400)
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            else
            {
                Height_Control(ULTRA_HOLD_HEIGHT);
                dir = 1;
                t_cnt2++;
                //次数达到即返回
                if (t_cnt2 >= 3)
                {
                    dir = 3;
                    t_cnt = 0;
                }
            }
            break;
        case 3:
            t_cnt++;
            if (t_cnt < 400)
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            else
            {
                dir = 4;
                t_cnt = 0;
            }
            break;
        case 4:
            //倒着回去
            except_A.y = FLY_ANGLE_B;
            except_A.x = angle_exp.x;

            if (camera_mode == CAM_TN)
            {
                t_flag = 1;
                except_A.y = angle_exp.y;
                except_A.x = angle_exp.x;
            }
            break;
        }
    }

    if (t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR;
        t_cnt++;
        if (t_cnt < 400)
        {
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
        }
        else
        {
            t_cnt = 1000;
            Task_Landing();
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            if (!fly_ready)
            {
                t_flag = 0;
                t_cnt = 0;
                Task_Finish_Flag = 1;
            }
        }
    }
}

//Y向悬停
void Task_Line_Hover_Y(void)
{
    if (ultra_distance <= CONTROL_HEIGHT && NS != 0)
    {
        //未达到高度，期望水平为0
        except_A.x = 0;
        except_A.y = 0;
    }
    else
    {
        //角度进行限制
        except_A.y = angle_exp.y;

        //前后用油门校偏差
        except_A.x = MAX_CTRL_ANGLE * (my_deathzoom((CH_filter[ROL]), 30) / 500.0f); //30
    }
}

//飞矩形
void Task_Fly_Rectangle(void)
{
    static u16 t_cnt = 0;          //计数器
    static u8 t_flag = 0;          //t的标志位 降落
    static u8 t_cur_dir = t_Dir_R; //飞行方向，右 前 左 下 右

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
        t_cur_dir = t_Dir_R;
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (ultra_distance > CONTROL_HEIGHT && !t_flag)
    {
        switch (t_cur_dir)
        {
        case t_Dir_R:
            //默认向右飞
            except_A.x = FLY_ANGLE_R;
            except_A.y = angle_exp.y;
            //判断转向
            if (camera_mode == CAM_XNYP)
            {
                t_cur_dir = t_Dir_F; //改变当前方向
                t_cnt = 0;
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            break;
        case t_Dir_F:
            //向前飞
            t_cnt++;
            if (t_cnt >= brake_time_R + 10)
            {
                except_A.y = -FLY_ANGLE_F;
                except_A.x = angle_exp.x;
                t_cnt = 10000;
            }
            else if (t_cnt >= brake_time_R)
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            else
            {
                except_A.x = angle_exp.x - brake_speed;
                except_A.y = angle_exp.y;
            }

            //判断转向
            if (camera_mode == CAM_XNYN)
            {
                t_cur_dir = t_Dir_L; //改变当前方向
                t_cnt = 0;
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            break;
        case t_Dir_L:
            //向左飞
            t_cnt++;
            if (t_cnt >= brake_time_F + 100)
            {
                except_A.x = -FLY_ANGLE_L;
                except_A.y = angle_exp.y;
                t_cnt = 1300;
            }
            else if (t_cnt >= brake_time_F)
            {
                except_A.x = 0;
                except_A.y = angle_exp.y;
                if (camera_mode != CAM_XNYN)
                    t_cnt = 1300;
            }
            else
            {
                except_A.x = angle_exp.x - brake_speed + 1.0f;
                except_A.y = angle_exp.y + brake_speed;
                // if (camera_mode != CAM_XNYN)
                //     t_cnt = 1300;
            }

            //判断转向
            if (camera_mode == CAM_XPYN)
            {
                t_cur_dir = t_Dir_B; //改变当前方向
                t_cnt = 0;
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            break;
        case t_Dir_B:
            t_cnt++;
            if (t_cnt >= brake_time_L + 100)
            {
                except_A.y = FLY_ANGLE_B;
                except_A.x = angle_exp.x;
                t_cnt = 1300;
            }
            else if (t_cnt >= brake_time_L)
            {
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
                if (camera_mode != CAM_XPYN)
                    t_cnt = 1300;
            }
            else
            {
                except_A.x = angle_exp.x + brake_speed + 2.5f;
                except_A.y = angle_exp.y + brake_speed;
                // if (camera_mode != CAM_XPYN)
                //     t_cnt = 1300;
            }

            //判断转向
            if (camera_mode == CAM_XPYP)
            {

                t_cur_dir = t_Dir_R2; //改变当前方向
                t_cnt = 0;
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            break;
        case t_Dir_R2:
            //第二次向右飞
            t_cnt++;
            if (t_cnt >= brake_time_B + 100)
            {
                except_A.x = FLY_ANGLE_R2;
                except_A.y = angle_exp.y;
                // t_cnt = 1000;
            }
            else if (t_cnt >= brake_time_B)
            {
                except_A.x = FLY_ANGLE_R2;
                except_A.y = angle_exp.y;
                if (camera_mode != CAM_XPYP)
                    t_cnt = brake_time_B + 100;
            }
            else
            {
                except_A.x = angle_exp.x + brake_speed + 2.0f;
                except_A.y = angle_exp.y - brake_speed;
                // if (camera_mode != CAM_XPYP)
                //     t_cnt = 1300;
            }

            //判断转向
            if ((camera_mode == CAM_TN || camera_mode == CAM_CROSS) && t_cnt >= brake_time_B + 300)
            {

                t_flag = 1; //准备降落
                t_cnt = 0;
                except_A.x = angle_exp.x;
                except_A.y = angle_exp.y;
            }
            break;
        }
    }

    if (t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR;
        t_cnt++;
        if (t_cnt < 400)
        {
            Height_Control(230);
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
        }
        else
        {
            t_cnt = 1200;
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            Task_Landing();
            if (!fly_ready)
            {
                t_flag = 0;
                t_cnt = 0;
                Task_Finish_Flag = 1;
            }
        }
    }
}

//定点悬停
void Task_Object_Hover(void)
{
    //static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_flag = 0;
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (ultra_distance <= CONTROL_HEIGHT && NS != 0)
    {
        //未达到高度，期望水平为0
        except_A.x = 0;
        except_A.y = 0;
    }
    else
    {
        except_A.x = angle_exp.x;
        except_A.y = angle_exp.y;
    }
}

//转向矩形
void Task_Rectangle_Yaw(void)
{
    static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落
    //static u8 t_cur_dir = t_Dir_R; //飞行方向，右 前 左 下 右
    static u16 t_turn = 0;
    static u8 t_flag2 = 0;

    if (!fly_ready && NS != 0)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
        t_turn = 0;
        t_flag2 = 0;
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (ultra_distance > CONTROL_HEIGHT && !t_flag)
    {
        if (yaw_angle == 0)
        {
            if (!t_turn)
            {
                except_A.y = -FLY_ANGLE_F;
                except_A.x = angle_exp.x;
            }

            if (camera_mode == CAM_XNYN && t_turn != 1)
            {
                t_turn = 1;
            }

            if (t_turn)
            {
                t_cnt++;
                if (!t_flag2)
                {
                    yaw_angle = -89;
                    t_flag2 = 1;
                }
                if (t_cnt >= 1000)
                {
                    t_turn = 0;
                    t_cnt = 0;
                    t_flag2 = 0;
                }
            }
        }
        else if (yaw_angle >= -30 && yaw_angle < 0)
        {
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y + 1.0f;
        }
        else
        {
            except_A.x = 0;
            except_A.y = 0;
        }
    }
}

//物体跟踪
void Task_Object_Track(void)
{
    //static u16 t_cnt = 0; //计数器
    static u8 t_flag = 0; //t的标志位 降落

    if (!fly_ready)
    {
        fly_ready = 1; //解锁
        t_flag = 0;
    }

    if (!t_flag)
    {
        My_Rc_Pwm_In[2] = FLY_THR; //保持定高
        Height_Control(ULTRA_HOLD_HEIGHT);
    }

    if (ultra_distance <= CONTROL_HEIGHT && NS != 0)
    {
        //未达到高度，期望水平为0
        except_A.x = 0;
        except_A.y = 0;
    }
    else
    {
        except_A.x = angle_exp.x;
        except_A.y = angle_exp.y;
    }
}

//起飞悬停
void Task_Fly_Hover(void)
{
    if (!fly_ready)
    {
        ultra_hold_height = ULTRA_HOLD_HEIGHT;
        fly_ready = 1; //解锁
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ultra_hold_height);

    if (ultra_distance <= 120 && NS != 0)
    {
        //未达到高度，期望水平为0
        except_A.x = 0;
        except_A.y = 0;
    }
    else
    {
        //使用摄像头定位
        except_A.x = angle_exp.x;
        except_A.y = angle_exp.y;
        // except_A.x = of_exp_1.x;
        // except_A.y = of_exp_1.y;
        // except_A.x = of_exp_2.x;
        // except_A.y = of_exp_2.y;
    }
}

void Task_Pick(void)
{
    EMPlug_Set(High);
}

void Task_Throw(void)
{
    EMPlug_Set(Low);
}

//切换后延时进行摄像头判断
u16 camera_delay_time = 300;

void Task_Fly_Forward(void)
{
    static u8 t_flag = 0;
    static u16 t_cnt = 0; //计数器

    if (!fly_ready)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ULTRA_HOLD_HEIGHT);

    //切换任务后所有标记清除
    if (Pre_Task != Cur_Task)
    {
        t_flag = 0;
        t_cnt = 0;
        Pre_Task = Cur_Task;
    }

    t_cnt++;

    //延时检测到图形后进入悬停
    if ((camera_mode == CAM_XNYN || camera_mode == CAM_TP) && t_cnt > camera_delay_time && !t_flag)
    {
        t_flag = 1;
        t_cnt = 0;
    }

    if (!t_flag)
    {
        //向前飞
        except_A.x = angle_exp.x;
        except_A.y = -FLY_ANGLE_F;
    }
    else
    {
        if (t_cnt > brake_time_F)
        {
            //悬停
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            t_cnt = brake_time_F + 1; //防止进入刹车
        }
        else
        {
            //刹车
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y + brake_speed;
        }
    }
}

void Task_Fly_Backward(void)
{
    static u8 t_flag = 0;
    static u16 t_cnt = 0; //计数器

    if (!fly_ready)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ULTRA_HOLD_HEIGHT);

    //切换任务后所有标记清除
    if (Pre_Task != Cur_Task)
    {
        t_flag = 0;
        t_cnt = 0;
        Pre_Task = Cur_Task;
    }

    t_cnt++;

    //延时检测到图形后进入悬停
    if ((camera_mode == CAM_XPYP || camera_mode == CAM_TN) && t_cnt > camera_delay_time && !t_flag)
    {
        t_flag = 1;
        t_cnt = 0;
    }

    if (!t_flag)
    {
        //向后飞
        except_A.x = angle_exp.x;
        except_A.y = FLY_ANGLE_B;
    }
    else
    {
        if (t_cnt > brake_time_B)
        {
            //悬停
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            t_cnt = brake_time_B + 1; //防止进入刹车
        }
        else
        {
            //刹车
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y - brake_speed;
        }
    }
}

void Task_Fly_Left(void)
{
    static u8 t_flag = 0;
    static u16 t_cnt = 0; //计数器

    if (!fly_ready)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ULTRA_HOLD_HEIGHT);

    //切换任务后所有标记清除
    if (Pre_Task != Cur_Task)
    {
        t_flag = 0;
        t_cnt = 0;
        Pre_Task = Cur_Task;
    }

    t_cnt++;

    //延时检测到图形后进入悬停
    if ((camera_mode == CAM_XPYN) && t_cnt > camera_delay_time && !t_flag)
    {
        t_flag = 1;
        t_cnt = 0;
    }

    if (!t_flag)
    {
        //向左飞
        except_A.x = -FLY_ANGLE_L;
        except_A.y = angle_exp.y;
    }
    else
    {
        if (t_cnt > brake_time_L)
        {
            //悬停
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            t_cnt = brake_time_L + 1; //防止进入刹车
        }
        else
        {
            //刹车
            except_A.x = angle_exp.x + brake_speed;
            except_A.y = angle_exp.y;
        }
    }
}

void Task_Fly_Right(void)
{
    static u8 t_flag = 0;
    static u16 t_cnt = 0; //计数器

    if (!fly_ready)
    {
        fly_ready = 1; //解锁
        t_cnt = 0;
        t_flag = 0;
    }

    My_Rc_Pwm_In[2] = FLY_THR; //保持定高
    Height_Control(ULTRA_HOLD_HEIGHT);

    //切换任务后所有标记清除
    if (Pre_Task != Cur_Task)
    {
        t_flag = 0;
        t_cnt = 0;
        Pre_Task = Cur_Task;
    }

    t_cnt++;

    //延时检测到图形后进入悬停
    if ((camera_mode == CAM_XNYP || camera_mode == CAM_TN) && t_cnt > camera_delay_time && !t_flag)
    {
        t_flag = 1;
        t_cnt = 0;
    }

    if (!t_flag)
    {
        //向右飞
        except_A.x = FLY_ANGLE_R;
        except_A.y = angle_exp.y;
    }
    else
    {
        if (t_cnt > brake_time_R)
        {
            //悬停
            except_A.x = angle_exp.x;
            except_A.y = angle_exp.y;
            t_cnt = brake_time_R + 1; //防止进入刹车
        }
        else
        {
            //刹车
            except_A.x = angle_exp.x - brake_speed;
            except_A.y = angle_exp.y;
        }
    }
}
