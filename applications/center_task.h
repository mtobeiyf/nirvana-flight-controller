#ifndef _CENTER_TASK_H
#define _CENTER_TASK_H

#include "include.h"
#include "ctrl.h"
#include "ultrasonic.h"
#include "center_ctrl.h"
#include "my_io.h"
#include "usart.h"
#include "protocol.h"

//串口：参数的更新

// #define FLY_ANGLE 0.10 //except.x/y的值
// #define FLY_ANGLE_R 0.70
// #define FLY_ANGLE_F 0.50
// #define FLY_ANGLE_L 0.60
// #define FLY_ANGLE_B 0.90

//投掷
extern float FLY_METAL_F;
extern float FLY_METAL_B;
extern u16 brake_metal_F;
extern u16 brake_metal_B;

extern float FLY_ANGLE_F;
extern float FLY_ANGLE_B;
extern float FLY_ANGLE_L;
extern float FLY_ANGLE_R;
extern float FLY_ANGLE_R2;

//光流预设速度
extern float set_speed_x;
extern float set_speed_y;
extern u8 use_of_2; //使用光流串级数据

extern u16 brake_time;
extern float brake_speed;
extern u16 brake_time_F;
extern u16 brake_time_B;
extern u16 brake_time_L;
extern u16 brake_time_R;

//-24~+24 右摇杆x负轴小于0 y正轴小于0
#define FLY_THR 1600 //定高油门

//任务枚举变量
enum Tasks
{
    t_Urgent_Stop = 0, //紧急停机
    t_Landing,         //自动降落
    t_Manual_Control,  //手动控制
    t_Line_Hover,      //线上悬停
    t_Line_Hover_Y,    //横向悬停
    t_Fly_A_To_B,      //由A飞到B
    t_Fly_Rectangle,   //绕矩形飞
    t_Throw_Metal,     //投掷铁片
    t_Pick_Metal,      //拾取铁片
    t_Object_Hover,    //定物悬停
    t_Object_Track,    //追踪物体
    t_Rectangle_Yaw,   //新版矩形
    t_Fly_Hover,       //起飞悬停
    t_Fly_Forward,     //前
    t_Fly_Backward,    //后
    t_Fly_Left,        //左
    t_Fly_Right,       //右
    t_Pick,            //捡铁片
    t_Throw            //丢铁片

};

extern u8 Rc_Func;          //遥控器功能按键
extern u8 Cur_Task;         //当前任务序号
extern u8 Cur_State;        //当前状态 留作控制信息判断参量
extern u8 Task_Finish_Flag; //当前任务完成标志
extern u8 Landing_Flag;     //降落标志

//更新任务状态
//通过该函数来改变全局变量任务序号值
//为不同参量输入形式保留
void Get_Task(void);

//执行任务
void Run_Task(void);

void My_Task(void);

void Task_Urgent_Stop(void); //急停任务
void Task_Landing(void);     //自动降落

enum t_Fly_Rectangle
{
    t_Dir_R = 0, //向右
    t_Dir_F,     //向前
    t_Dir_L,     //向左
    t_Dir_B,     //向后
    t_Dir_R2     //第二次向右 准备降落
};

//对应的任务输出
void Task_Manual_Control(void);
void Task_Line_Hover(void);
void Task_Fly_A_To_B(void);
void Task_Line_Hover_Y(void);
void Task_Fly_Rectangle(void);
void Task_Throw_Metal(void);
void Task_Pick_Metal(void);
void Task_Object_Hover(void);
void Task_Object_Track(void);
void Task_Rectangle_Yaw(void);

void Task_Fly_Hover(void);
void Task_Fly_Forward(void);
void Task_Fly_Backward(void);
void Task_Fly_Left(void);
void Task_Fly_Right(void);
void Task_Pick(void);
void Task_Throw(void);

#endif
