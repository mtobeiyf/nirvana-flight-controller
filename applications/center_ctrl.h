#ifndef _CENTER_CTRL_H
#define _CENTER_CTRL_H

#include "pwm_in.h"
#include "rc.h"
#include "ultrasonic.h"
#include "ctrl.h"
#include "filter.h"
#include "ak8975.h"
#include "center_task.h"

//================油门电机部分===============
extern u16 My_Rc_Pwm_In[8];
// #define USE_KV1400 //使用KV1400的电机
#define MOTOR_PROP 1 //油门输出比例
//=======================================

//================定高部分=================
#define ULTRA_HOLD_HEIGHT 350 //定高高度 初始值
#define ULTRA_HOLD_SPEED 300  //定高最大速度 初始值

#define CONTROL_HEIGHT 120 //高度阈值 在此之下启动初始化

extern u16 ultra_hold_height; //超声波定高高度 mm
extern u16 ultra_hold_speed;  //定高速度 mm/s

void Height_Control(u16 height); //定高
//=======================================

//================YAW部分=================
extern s16 yaw_angle;   //航偏角改变量
void Yaw_Control(void); //航偏控制
//=======================================

//===============传感器校准=================
#define MPU_ACC 1              //加速度计
#define MPU_GYRO 2             //陀螺仪
#define MAG_COMP 3             //磁力罗盘
void Sensor_Calibrate(u8 num); //校准传感器
//=======================================

//===============摄像头部分=================

//摄像头模式宏定义
#define CAM_NONE 0  //空
#define CAM_Y 11    //直行
#define CAM_X 12    //横行
#define CAM_XNYN 13 //前左
#define CAM_XPYN 14 //前右
#define CAM_XNYP 15 //后左
#define CAM_XPYP 16 //后右
#define CAM_TP 21   //正T
#define CAM_TN 22   //倒T
#define CAM_CROSS 3 //倒T

//摄像头寻物
#define CAM_TARGET_HIT 1  //有目标
#define CAM_TARGET_MISS 0 //无目标

//摄像头任务模式
#define CAM_TRACK_XY 11       //横竖跟踪
#define CAM_TRACK_COLOR 21    //颜色学习跟踪
#define CAM_TRACK_TAG 12      //Apriltag跟踪
#define CAM_TRACK_OBJECT_G 12 //灰度跟踪

//接收摄像头数据
extern u8 camera_offset_x; //x方向偏移
extern u8 camera_offset_y; //y方向偏移
extern u8 camera_mode;     //摄像头返回模式

void Camera_Location_Update(void); //摄像头位置信息更新

typedef struct
{
    float camera_delta_xx_old;
    float camera_delta_yy_old;

    float camera_delta_xx;
    float camera_delta_yy;

    float camera_speed_xx;
    float camera_speed_yy;

    float update_time;
    float update_time_old;
    float update_time_now;
} Camera_Receive_Data;

extern Camera_Receive_Data Camera_Location_Data; //定位数据
extern u16 Camera_delta_update;

#define PID_GROUP_NUM 2
#define INTEGRAL1 100 //偏差限幅
#define INTEGRAL2 20  //X,Y方向积分限幅
#define SPEED_INTEGRAL1 170
#define SPEED_INTEGRAL2 150

typedef struct
{
    float x;
    float y;
} z_xy_f_t;

enum
{
    X,
    Y
};

typedef struct
{
    z_xy_f_t err;
    z_xy_f_t err_old;
    z_xy_f_t err_i;
    z_xy_f_t err_d;
    z_xy_f_t out;
    pid_t PID[2];
} z_location_pid;

extern z_location_pid eliminate_xy[PID_GROUP_NUM];

extern z_xy_f_t angle_exp; //期望角度值

void Angle_Control(void); //摄像头计算 输出期望

//=======================================

//===============高度PID部分===============
typedef struct
{
    float err;
    float err_old;
    float err_i;
    float err_d;
    float out;
    pid_t PID;

} height_ctrl_t;

extern height_ctrl_t eliminate_h;
void Height_PID_Control(float T, u16 height_exp); //高度控制
//=======================================

//=================光流部分=================

//光流PID数据
typedef struct
{
    float dx_old;
    float dy_old;

    float dx;
    float dy;

    float speed_x;
    float speed_y;

    float update_time;
    float update_time_old;
    float update_time_now;
} OF_Receive_Data;

extern OF_Receive_Data Optical_Flow_Data; //光流原始数据

extern z_location_pid of_xy_1[1]; //光流PID
extern z_location_pid of_xy_2[2]; //光流PID
extern z_xy_f_t of_exp_1;         //光流姿态期望-单级
extern z_xy_f_t of_exp_2;         //光流姿态期望-串级

extern vs16 optical_flow_dx;
extern vs16 optical_flow_dy;

void OF_Data_Receive_Prepare(u8 data);          //光流数据协议
void OF_Data_Receive_Anl(u8 *data_buf, u8 num); //光流数据解析

void Optical_Flow_PID_Init(void);  //PID参数的初始化
void Optical_Flow_Control_1(void); //光流单级解算
void Optical_Flow_Control_2(void); //光流串级解算

//=======================================

#endif
