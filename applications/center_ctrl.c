#include "center_ctrl.h"
#include "ak8975.h"

// 0横滚，1俯仰，2油门，3航向
u16 My_Rc_Pwm_In[8] = {1500, 1500, 1100, 1500, 1900, 1500, 1100, 1500};

//初始化定高值
u16 ultra_hold_height = ULTRA_HOLD_HEIGHT;
u16 ultra_hold_speed = ULTRA_HOLD_SPEED;

u16 height_old = ULTRA_HOLD_HEIGHT;
void Height_Control(u16 height)
{
	static u16 h_cnt = 0;
	if (height != height_old)
	{
		if (height > height_old && height <= ULTRA_HOLD_HEIGHT)
		{
			ultra_hold_height = height;
			height_old = height;
		}
		else if (height < height_old)
		{
			//300-1200ms
			h_cnt++;
			if (h_cnt == 1)
			{
				ultra_hold_height -= 2;
				height_old = ultra_hold_height;
				h_cnt = 0;
			}
		}
	}
	else
	{
		height_old = height;
	}
}

//改一次，自动调用，实现转向
s16 yaw_angle = 0;
void Yaw_Control(void)
{
	static u16 y_cnt = 0;
	if (yaw_angle != 0)
	{
		if (yaw_angle > 0)
		{
			y_cnt++;
			if (y_cnt == 5)
			{
				except_A.z = To_180_degrees(except_A.z + 1);
				yaw_angle -= 1;
				y_cnt = 0;
				if (yaw_angle <= 0)
				{
					yaw_angle = 0;
				}
			}
		}
		else if (yaw_angle < 0)
		{
			y_cnt++;
			if (y_cnt == 5)
			{
				except_A.z = To_180_degrees(except_A.z - 1);
				yaw_angle += 1;
				y_cnt = 0;
				if (yaw_angle >= 0)
				{
					yaw_angle = 0;
				}
			}
		}
	}
	else
	{
		y_cnt = 0;
	}
}

//传感器校准函数
void Sensor_Calibrate(u8 num)
{
	switch (num)
	{
	case MPU_ACC:
		if (mpu6050.Acc_CALIBRATE != 1)
			mpu6050.Acc_CALIBRATE = 1;
		break;
	case MPU_GYRO:
		if (mpu6050.Gyro_CALIBRATE != 1)
			mpu6050.Gyro_CALIBRATE = 1;
		break;
	case MAG_COMP:
		if (Mag_CALIBRATED != 1)
			Mag_CALIBRATED = 1;
		break;
	default:
		break;
	}
}

//摄像头数据
u8 camera_offset_x = 0;
u8 camera_offset_y = 0;
u8 camera_mode = 0;

Camera_Receive_Data Camera_Location_Data; //定位数据
u16 Camera_delta_update = 0;

void Camera_Location_Update()
{
	if (ultra_distance > CONTROL_HEIGHT)
	{
		//位置
		Camera_Location_Data.camera_delta_xx = (100 - camera_offset_x) * 0.1;
		Camera_Location_Data.camera_delta_yy = (camera_offset_y - 100) * 0.1;

		//时间
		Camera_Location_Data.update_time_now = GetSysTime_us() / 1000000.0f;
		Camera_Location_Data.update_time = Camera_Location_Data.update_time_now - Camera_Location_Data.update_time_old;
		Camera_Location_Data.update_time_old = Camera_Location_Data.update_time_now;
		//首次修正
		if (Camera_delta_update == 0)
		{
			Camera_Location_Data.update_time = 0.025f;
		}

		//速度
		Camera_Location_Data.camera_speed_xx = (Camera_Location_Data.camera_delta_xx - Camera_Location_Data.camera_delta_xx_old) / Camera_Location_Data.update_time;
		Camera_Location_Data.camera_speed_yy = (Camera_Location_Data.camera_delta_yy - Camera_Location_Data.camera_delta_yy_old) / Camera_Location_Data.update_time;

		Camera_Location_Data.camera_delta_xx_old = Camera_Location_Data.camera_delta_xx;
		Camera_Location_Data.camera_delta_yy_old = Camera_Location_Data.camera_delta_yy;

		Angle_Control();

		Camera_delta_update = 1;
	}
	else
	{
		except_A.x = 0;
		except_A.y = 0;
		eliminate_xy[0].err_i.x = 0;
		eliminate_xy[0].err_i.y = 0;
		Camera_Location_Data.camera_delta_xx_old = 0;
		Camera_Location_Data.camera_delta_yy_old = 0;
		Camera_delta_update = 0;
	}
}

//视角偏移修正
void correct_pixel(void)
{
	float a = (float)ultra_distance / 1200.f;
	Camera_Location_Data.camera_delta_xx = Camera_Location_Data.camera_delta_xx * a;
	Camera_Location_Data.camera_delta_yy = Camera_Location_Data.camera_delta_yy * a;
}

z_location_pid eliminate_xy[PID_GROUP_NUM];
z_xy_f_t angle_exp;

//串级PID控制位置
void Angle_Control()
{
	float T = Camera_Location_Data.update_time;

	////////////////////////////////////////位置环///////////////////////////////////////////////////////////

	eliminate_xy[0].PID[X].kp = pid_setup.groups.hc_sp.kp;
	eliminate_xy[0].PID[X].ki = pid_setup.groups.hc_sp.ki;
	eliminate_xy[0].PID[X].kd = pid_setup.groups.hc_sp.kd;

	eliminate_xy[0].PID[Y].kp = pid_setup.groups.hc_sp.kp;
	eliminate_xy[0].PID[Y].ki = pid_setup.groups.hc_sp.ki;
	eliminate_xy[0].PID[Y].kd = pid_setup.groups.hc_sp.kd;

	// eliminate_xy[0].err.x = Moving_Median(2, 3, Camera_Location_Data.camera_delta_xx);
	// eliminate_xy[0].err.y = Moving_Median(3, 3, Camera_Location_Data.camera_delta_yy);
	// Camera_Location_Data.camera_speed_xx = Moving_Median(4, 3, Camera_Location_Data.camera_speed_xx);
	// Camera_Location_Data.camera_speed_yy = Moving_Median(5, 3, Camera_Location_Data.camera_speed_yy);
	eliminate_xy[0].err.x = Camera_Location_Data.camera_delta_xx;
	eliminate_xy[0].err.y = Camera_Location_Data.camera_delta_yy;

	//微分
	eliminate_xy[0].err_d.x = 0.025f / T * eliminate_xy[0].PID[X].kd * (eliminate_xy[0].err.x - eliminate_xy[0].err_old.x); //离散时间间隔取1，需要系数进行调节
	eliminate_xy[0].err_d.y = 0.025f / T * eliminate_xy[0].PID[Y].kd * (eliminate_xy[0].err.y - eliminate_xy[0].err_old.y);

	//积分
	//	eliminate_xy[0].err_d.x = DeathRoom(eliminate_xy[0].err_d.x,12.f);   //小范围内设置死区
	//	eliminate_xy[0].err_d.y = DeathRoom(eliminate_xy[0].err_d.y,12.f);
	eliminate_xy[0].err_i.x += eliminate_xy[0].PID[X].ki * eliminate_xy[0].err.x * T;
	eliminate_xy[0].err_i.y += eliminate_xy[0].PID[Y].ki * eliminate_xy[0].err.y * T;

	//误差限幅
	eliminate_xy[0].err.x = LIMIT(eliminate_xy[0].err.x, -INTEGRAL1, INTEGRAL1);
	eliminate_xy[0].err.y = LIMIT(eliminate_xy[0].err.y, -INTEGRAL1, INTEGRAL1);

	//积分限幅
	eliminate_xy[0].err_i.x = LIMIT(eliminate_xy[0].err_i.x, -INTEGRAL2, INTEGRAL2);
	eliminate_xy[0].err_i.y = LIMIT(eliminate_xy[0].err_i.y, -INTEGRAL2, INTEGRAL2);

	//偏差位移控制输出(期望速度)
	eliminate_xy[0].out.x =
		-(eliminate_xy[0].PID[X].kp * (eliminate_xy[0].err.x + eliminate_xy[0].err_d.x + eliminate_xy[0].err_i.x));
	eliminate_xy[0].out.y =
		-(eliminate_xy[0].PID[Y].kp * (eliminate_xy[0].err.y + eliminate_xy[0].err_d.y + eliminate_xy[0].err_i.y));

	eliminate_xy[0].err_old.x = eliminate_xy[0].err.x;
	eliminate_xy[0].err_old.y = eliminate_xy[0].err.y;

	//////////////////////////////////////////速度环/////////////////////////////////////////////////////////

	eliminate_xy[1].PID[X].kp = pid_setup.groups.hc_height.kp;
	eliminate_xy[1].PID[X].ki = pid_setup.groups.hc_height.ki;
	eliminate_xy[1].PID[X].kd = pid_setup.groups.hc_height.kd;

	eliminate_xy[1].PID[Y].kp = pid_setup.groups.hc_height.kp;
	eliminate_xy[1].PID[Y].ki = pid_setup.groups.hc_height.ki;
	eliminate_xy[1].PID[Y].kd = pid_setup.groups.hc_height.kd;

	eliminate_xy[1].err.x = (Camera_Location_Data.camera_speed_xx - eliminate_xy[0].out.x);
	eliminate_xy[1].err.y = (Camera_Location_Data.camera_speed_yy - eliminate_xy[0].out.y);

	eliminate_xy[1].err_d.x = 0.025f / T * eliminate_xy[1].PID[X].kd * (eliminate_xy[1].err.x - eliminate_xy[1].err_old.x);
	eliminate_xy[1].err_d.y = 0.025f / T * eliminate_xy[1].PID[Y].kd * (eliminate_xy[1].err.y - eliminate_xy[1].err_old.y);

	eliminate_xy[1].err_i.x += eliminate_xy[1].PID[X].ki * eliminate_xy[1].err.x * T;
	eliminate_xy[1].err_i.y += eliminate_xy[1].PID[Y].ki * eliminate_xy[1].err.y * T;

	eliminate_xy[1].err.x = LIMIT(eliminate_xy[1].err.x, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);
	eliminate_xy[1].err.y = LIMIT(eliminate_xy[1].err.y, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);

	eliminate_xy[1].err_i.x = LIMIT(eliminate_xy[1].err_i.x, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);
	eliminate_xy[1].err_i.y = LIMIT(eliminate_xy[1].err_i.y, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);

	eliminate_xy[1].out.x =
		-eliminate_xy[1].PID[X].kp * (eliminate_xy[1].err.x + eliminate_xy[1].err_d.x + eliminate_xy[1].err_i.x);
	eliminate_xy[1].out.y =
		eliminate_xy[1].PID[Y].kp * (eliminate_xy[1].err.y + eliminate_xy[1].err_d.y + eliminate_xy[1].err_i.y);

	if (Cur_Task != t_Manual_Control) //无遥控器控制时按自动模式输出
	{
		// except_A.x = LIMIT(eliminate_xy[1].out.x, -7.f, 7.f);
		// except_A.y = LIMIT(eliminate_xy[1].out.y, -7.f, 7.f);
		angle_exp.x = LIMIT(eliminate_xy[1].out.x, -7.f, 7.f);
		angle_exp.y = LIMIT(eliminate_xy[1].out.y, -7.f, 7.f);
	}

	eliminate_xy[1].err_old.x = eliminate_xy[1].err.x;
	eliminate_xy[1].err_old.y = eliminate_xy[1].err.y;
}

height_ctrl_t eliminate_h;
void Height_PID_Control(float T, u16 height_exp)
{
	eliminate_h.err = ultra_distance - (float)height_exp;

	eliminate_h.PID.kp = 2.20f;
	eliminate_h.PID.ki = 0.00f;
	eliminate_h.PID.kd = 0.15f;

	eliminate_h.err_d = 0.002f / T * eliminate_h.PID.kd * (eliminate_h.err - eliminate_h.err_old);
	eliminate_h.err_i += T * eliminate_h.PID.ki * eliminate_h.err;

	eliminate_h.err = LIMIT(eliminate_h.err, -50, 50);
	eliminate_h.err_i = LIMIT(eliminate_h.err_i, -70, 70);

	eliminate_h.out = 1550 - eliminate_h.PID.kp * (eliminate_h.err + eliminate_h.err_d + eliminate_h.err_i);

	Rc_Pwm_In[THR] = LIMIT(eliminate_h.out, 1100, 1700);

	eliminate_h.err_old = eliminate_h.err;
}

//光流数据初始化
vs16 optical_flow_dx = 0;
vs16 optical_flow_dy = 0;

//积分适用数据
vs16 optical_flow_dx_fix = 0;
vs16 optical_flow_dy_fix = 0;

u16 optical_delta_update = 0;

#define SINGLE_PID
vs16 dxoffset = 0;

OF_Receive_Data Optical_Flow_Data;

void Optical_Flow_Update(void)
{
	if (ultra_distance > CONTROL_HEIGHT)
	{
		//时间更新
		Optical_Flow_Data.update_time_now = GetSysTime_us() / 1000000.0f;
		Optical_Flow_Data.update_time = Optical_Flow_Data.update_time_now - Optical_Flow_Data.update_time_old;
		Optical_Flow_Data.update_time_old = Optical_Flow_Data.update_time_now;

		//首次校正
		if (optical_delta_update == 0)
		{
			Optical_Flow_Data.update_time = 0.02f;
		}

		//速度
		// Optical_Flow_Data.speed_x = (Optical_Flow_Data.dx - Optical_Flow_Data.dx_old) / Optical_Flow_Data.update_time;
		// Optical_Flow_Data.speed_y = (Optical_Flow_Data.dy - Optical_Flow_Data.dy_old) / Optical_Flow_Data.update_time;
		// Optical_Flow_Data.speed_x = -optical_flow_dy * 0.1;
		// Optical_Flow_Data.speed_y = -(optical_flow_dx + dxoffset) * 0.1;
		Optical_Flow_Data.speed_x = -optical_flow_dy_fix * 0.1;
		Optical_Flow_Data.speed_y = -optical_flow_dx_fix * 0.1;

		//位置更新
		Optical_Flow_Data.dx += -optical_flow_dy_fix * 0.1 * Optical_Flow_Data.update_time;
		Optical_Flow_Data.dy += -optical_flow_dx_fix * 0.1 * Optical_Flow_Data.update_time;

		Optical_Flow_Data.dx_old = Optical_Flow_Data.dx;
		Optical_Flow_Data.dy_old = Optical_Flow_Data.dy;

		//默认开启单级速度控制
		Optical_Flow_Control_1();
		//使用串级 注意积分清零
		if (use_of_2)
		{
			Optical_Flow_Control_2();
		}
		else
		{
			//积分清零
			of_xy_2[0].err_i.x = 0;
			of_xy_2[0].err_i.y = 0;
			of_xy_2[1].err_i.x = 0;
			of_xy_2[1].err_i.y = 0;
		}

		optical_delta_update = 1;
	}
	else
	{
		Optical_Flow_PID_Init(); //PID参数的全部初始化

		of_exp_1.x = 0.0f;
		of_exp_1.y = 0.0f;
		of_exp_2.x = 0.0f;
		of_exp_2.y = 0.0f;

		of_xy_1[0].err_i.x = 0;
		of_xy_1[0].err_i.y = 0;

		of_xy_2[0].err_i.x = 0;
		of_xy_2[0].err_i.y = 0;
		of_xy_2[1].err_i.x = 0;
		of_xy_2[1].err_i.y = 0;

		Optical_Flow_Data.dx_old = 0;
		Optical_Flow_Data.dy_old = 0;
		optical_delta_update = 0;
	}
}

void OF_Data_Receive_Prepare(u8 data)
{
	static u8 RxBuffer[50];
	static u8 _data_len = 0, _data_cnt = 0;
	static u8 state = 0;

	if (state == 0 && data == 0xAA)
	{
		state = 1;
		RxBuffer[0] = data;
	}
	else if (state == 1 && data == 0xAA)
	{
		state = 2;
		RxBuffer[1] = data;
	}
	else if (state == 2 && data < 0xF1)
	{
		state = 3;
		RxBuffer[2] = data;
	}
	else if (state == 3 && data < 50)
	{
		state = 4;
		RxBuffer[3] = data;
		_data_len = data;
		_data_cnt = 0;
	}
	else if (state == 4 && _data_len > 0)
	{
		_data_len--;
		RxBuffer[4 + _data_cnt++] = data;
		if (_data_len == 0)
			state = 5;
	}
	else if (state == 5)
	{
		state = 0;
		RxBuffer[4 + _data_cnt] = data;
		OF_Data_Receive_Anl(RxBuffer, _data_cnt + 5);
	}
	else
		state = 0;
}

void OF_Data_Receive_Anl(u8 *data_buf, u8 num)
{
	u8 sum = 0;
	for (u8 i = 0; i < (num - 1); i++)
		sum += *(data_buf + i);
	if (!(sum == *(data_buf + num - 1)))
		return; //判断sum
	if (!(*(data_buf) == 0xAA && *(data_buf + 1) == 0xAA))
		return; //判断帧头

	//融合后的数据
	if (*(data_buf + 2) == 0x51 && *(data_buf + 4) == 1)
	{
		optical_flow_dx = (vs16)(*(data_buf + 6) << 8) | *(data_buf + 7);
		optical_flow_dy = (vs16)(*(data_buf + 8) << 8) | *(data_buf + 9);
		optical_flow_dx_fix = (vs16)(*(data_buf + 10) << 8) | *(data_buf + 11);
		optical_flow_dy_fix = (vs16)(*(data_buf + 12) << 8) | *(data_buf + 13);
		Optical_Flow_Update();
	}
}

//单级PID
z_location_pid of_xy_1[1];
z_xy_f_t of_exp_1;

//串级
z_location_pid of_xy_2[2];
z_xy_f_t of_exp_2;

void Optical_Flow_PID_Init(void)
{
	//单级
	//速度环
	of_xy_1[0].PID[X].kp = 1.50f;
	of_xy_1[0].PID[X].ki = 0.01f;
	of_xy_1[0].PID[X].kd = 0.50f;

	of_xy_1[0].PID[Y].kp = of_xy_1[0].PID[X].kp;
	of_xy_1[0].PID[Y].ki = of_xy_1[0].PID[X].ki;
	of_xy_1[0].PID[Y].kd = of_xy_1[0].PID[X].kd;

	//串级
	//位置环
	of_xy_2[0].PID[X].kp = pid_setup.groups.hc_sp.kp;
	of_xy_2[0].PID[X].ki = pid_setup.groups.hc_sp.ki;
	of_xy_2[0].PID[X].kd = pid_setup.groups.hc_sp.kd;

	of_xy_2[0].PID[Y].kp = pid_setup.groups.hc_sp.kp;
	of_xy_2[0].PID[Y].ki = pid_setup.groups.hc_sp.ki;
	of_xy_2[0].PID[Y].kd = pid_setup.groups.hc_sp.kd;

	//速度环
	of_xy_2[1].PID[X].kp = pid_setup.groups.hc_height.kp;
	of_xy_2[1].PID[X].ki = pid_setup.groups.hc_height.ki;
	of_xy_2[1].PID[X].kd = pid_setup.groups.hc_height.kd;

	of_xy_2[1].PID[Y].kp = pid_setup.groups.hc_height.kp;
	of_xy_2[1].PID[Y].ki = pid_setup.groups.hc_height.ki;
	of_xy_2[1].PID[Y].kd = pid_setup.groups.hc_height.kd;
}

//单级控制
void Optical_Flow_Control_1(void)
{
	float T = Optical_Flow_Data.update_time;

	//误差
	of_xy_1[0].err.x = (Optical_Flow_Data.speed_x - set_speed_x);
	of_xy_1[0].err.y = (Optical_Flow_Data.speed_y - set_speed_y);

	//微分
	of_xy_1[0].err_d.x = 0.02f / T * of_xy_1[0].PID[X].kd * (of_xy_1[0].err.x - of_xy_1[0].err_old.x);
	of_xy_1[0].err_d.y = 0.02f / T * of_xy_1[0].PID[Y].kd * (of_xy_1[0].err.y - of_xy_1[0].err_old.y);

	//积分
	of_xy_1[0].err_i.x += of_xy_1[0].PID[X].ki * of_xy_1[0].err.x * T;
	of_xy_1[0].err_i.y += of_xy_1[0].PID[Y].ki * of_xy_1[0].err.y * T;

	//误差限幅
	of_xy_1[0].err.x = LIMIT(of_xy_1[0].err.x, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);
	of_xy_1[0].err.y = LIMIT(of_xy_1[0].err.y, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);

	//积分限幅
	of_xy_1[0].err_i.x = LIMIT(of_xy_1[0].err_i.x, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);
	of_xy_1[0].err_i.y = LIMIT(of_xy_1[0].err_i.y, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);

	of_xy_1[0].out.x =
		-(of_xy_1[0].PID[X].kp * (of_xy_1[0].err.x + of_xy_1[0].err_d.x + of_xy_1[0].err_i.x));
	of_xy_1[0].out.y =
		-(of_xy_1[0].PID[Y].kp * (of_xy_1[0].err.y + of_xy_1[0].err_d.y + of_xy_1[0].err_i.y));

	//output
	of_exp_1.x = LIMIT(of_xy_1[0].out.x, -6.f, 6.f);
	of_exp_1.y = LIMIT(of_xy_1[0].out.y, -6.f, 6.f);

	of_xy_1[0].err_old.x = of_xy_1[0].err.x;
	of_xy_1[0].err_old.y = of_xy_1[0].err.y;
}

//串级控制
void Optical_Flow_Control_2(void)
{
	float T = Optical_Flow_Data.update_time;

	of_xy_2[0].err.x = Optical_Flow_Data.dx;
	of_xy_2[0].err.y = Optical_Flow_Data.dy;

	//微分
	of_xy_2[0].err_d.x = 0.02f / T * of_xy_2[0].PID[X].kd * (of_xy_2[0].err.x - of_xy_2[0].err_old.x);
	of_xy_2[0].err_d.y = 0.02f / T * of_xy_2[0].PID[Y].kd * (of_xy_2[0].err.y - of_xy_2[0].err_old.y);

	//积分
	of_xy_2[0].err_i.x += of_xy_2[0].PID[X].ki * of_xy_2[0].err.x * T;
	of_xy_2[0].err_i.y += of_xy_2[0].PID[Y].ki * of_xy_2[0].err.y * T;

	//误差限幅
	of_xy_2[0].err.x = LIMIT(of_xy_2[0].err.x, -INTEGRAL1, INTEGRAL1);
	of_xy_2[0].err.y = LIMIT(of_xy_2[0].err.y, -INTEGRAL1, INTEGRAL1);

	//积分限幅
	of_xy_2[0].err_i.x = LIMIT(of_xy_2[0].err_i.x, -INTEGRAL2, INTEGRAL2);
	of_xy_2[0].err_i.y = LIMIT(of_xy_2[0].err_i.y, -INTEGRAL2, INTEGRAL2);

	//位置偏差控制输出
	of_xy_2[0].out.x =
		-(of_xy_2[0].PID[X].kp * (of_xy_2[0].err.x + of_xy_2[0].err_d.x + of_xy_2[0].err_i.x));
	of_xy_2[0].out.y =
		-(of_xy_2[0].PID[Y].kp * (of_xy_2[0].err.y + of_xy_2[0].err_d.y + of_xy_2[0].err_i.y));

	of_xy_2[0].err_old.x = of_xy_2[0].err.x;
	of_xy_2[0].err_old.y = of_xy_2[0].err.y;

	//速度环
	of_xy_2[1].err.x = (Optical_Flow_Data.speed_x - of_xy_2[0].out.x);
	of_xy_2[1].err.y = (Optical_Flow_Data.speed_y - of_xy_2[0].out.y);

	//微分
	of_xy_2[1].err_d.x = 0.02f / T * of_xy_2[1].PID[X].kd * (of_xy_2[1].err.x - of_xy_2[1].err_old.x);
	of_xy_2[1].err_d.y = 0.02f / T * of_xy_2[1].PID[Y].kd * (of_xy_2[1].err.y - of_xy_2[1].err_old.y);

	//积分
	of_xy_2[1].err_i.x += of_xy_2[1].PID[X].ki * of_xy_2[1].err.x * T;
	of_xy_2[1].err_i.y += of_xy_2[1].PID[Y].ki * of_xy_2[1].err.y * T;

	//误差限幅
	of_xy_2[1].err.x = LIMIT(of_xy_2[1].err.x, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);
	of_xy_2[1].err.y = LIMIT(of_xy_2[1].err.y, -SPEED_INTEGRAL1, SPEED_INTEGRAL1);

	//积分限幅
	of_xy_2[1].err_i.x = LIMIT(of_xy_2[1].err_i.x, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);
	of_xy_2[1].err_i.y = LIMIT(of_xy_2[1].err_i.y, -SPEED_INTEGRAL2, SPEED_INTEGRAL2);

	of_xy_2[1].out.x =
		-(of_xy_2[1].PID[X].kp * (of_xy_2[1].err.x + of_xy_2[1].err_d.x + of_xy_2[1].err_i.x));
	of_xy_2[1].out.y =
		-(of_xy_2[1].PID[Y].kp * (of_xy_2[1].err.y + of_xy_2[1].err_d.y + of_xy_2[1].err_i.y));

	//output
	of_exp_2.x = LIMIT(of_xy_2[1].out.x, -7.f, 7.f);
	of_exp_2.y = LIMIT(of_xy_2[1].out.y, -7.f, 7.f);

	of_xy_2[1].err_old.x = of_xy_2[1].err.x;
	of_xy_2[1].err_old.y = of_xy_2[1].err.y;
}
