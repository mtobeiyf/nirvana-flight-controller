#include "protocol.h"


int CalCrc(int crc, u8 *buf, int len)
{
    unsigned int byte;
    unsigned char k;
    unsigned short ACC,TOPBIT;
//    unsigned short remainder = 0x0000;
    unsigned short remainder = crc;
    TOPBIT = 0x8000;
    for (byte = 0; byte < len; ++byte)
    {
        ACC = buf[byte];
        remainder ^= (ACC <<8);
        for (k = 8; k > 0; --k)
        {
            if (remainder & TOPBIT)
            {
                remainder = (remainder << 1) ^0x8005;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }
    }
    remainder=remainder^0x0000;
    return remainder;
}

u8 rc_rxbuf1[RC_RX_LENGTH];
u8 rc_rxbuf2[RC_RX_LENGTH];
u8 *rc_rxbuf=rc_rxbuf1;
u8 *rc_rxbuf_new=rc_rxbuf2;
u8 signal_strength=0;
u8 signal_loss=0;

void RxData(u8 temp,u8 length)
{
	static u8 num = 0;
	u8 *rc_rxbuf_temp;
	static u8 seq=0;
	static u8 signal_strength_counter=0;
	
	if( temp == 0xff )// 当接收到的值等于0XFF时，从头接收
	{
		rc_rxbuf_new[NUM_STX]=0xff;
		num=1;
		
		if(CalCrc('m',rc_rxbuf_new,length)==0)//计算得到的16位CRC校验码,slave
		{
			rc_rxbuf_temp=rc_rxbuf;
			rc_rxbuf=rc_rxbuf_new;
			rc_rxbuf_new=rc_rxbuf_temp;
		
			if(seq<=50)
			{
				signal_strength_counter=0;
			}
			else if(seq>150)
			{
				signal_strength=signal_strength_counter/100.0f;
			}
			else if(seq==rc_rxbuf[NUM_SEQ]-1)
			{
				signal_strength_counter++;
			}
			seq=rc_rxbuf[NUM_SEQ];

			signal_loss=0;
		}
	}
	else if(num<length)
	{
		rc_rxbuf_new[num]=temp;
		num++;
	}
}

u8 rc_txbuf[RC_TX_LENGTH];
void TxData(u8 *rc_txbuf,u8 length)
{
	static u8 seq=0;
	int crc;
	
	rc_txbuf[NUM_STX]=0xff;
	rc_txbuf[NUM_LEN]=length;
	rc_txbuf[NUM_SEQ]=seq;
	if(seq==255)
		seq=0;
	else
		seq++;

	crc = CalCrc('s',rc_txbuf, length-2);//计算得到的16位CRC校验码,master
	rc_txbuf[length-1]=(char)crc;//取校验码的低八位
	rc_txbuf[length-2]=(char)(crc >> 8);//取校验码的高八位
}
