#ifndef _PROTOCOL_H
#define _PROTOCOL_H
#include "stm32f4xx.h"


enum
{
	NUM_STX,
	NUM_LEN,
	NUM_SEQ
};


//在接收缓存中顺序
enum
{
	NUM_TX_STX,
	NUM_TX_LEN,
	NUM_TX_SEQ,
	
	
	NUM_pitch,
	NUM_roll,
	NUM_yaw,
	NUM_height,
	NUM_motor1,
	NUM_motor2,
	NUM_motor3,
	NUM_motor4,
	NUM_battery,

	NUM_plane_l_x,
	NUM_plane_l_y,
	NUM_plane_r_x,
	NUM_plane_r_y,

	NUM_camera_mode,
	NUM_camera_x,
	NUM_camera_y,
	
	
	NUM_TX_CKA,
	NUM_TX_CKB,
	
	RC_TX_LENGTH
};


//在发送缓存中顺序
enum
{
	NUM_RX_STX,
	NUM_RX_LEN,
	NUM_RX_SEQ,
	
	
	NUM_rocker_l_x,
	NUM_rocker_l_y,
	NUM_rocker_r_x,
	NUM_rocker_r_y,
	NUM_encoder1_counter,
	NUM_encoder2_counter,
	NUM_encoder3_counter,
	NUM_mode,
	NUM_function,
	NUM_parameter,
	
	
	NUM_RX_CKA,
	NUM_RX_CKB,
	
	RC_RX_LENGTH
};


extern u8 rc_txbuf[RC_TX_LENGTH];
extern u8 *rc_rxbuf;
extern u8 signal_strength;
extern u8 signal_loss;

int CalCrc(int crc, u8 *buf, int len);

void RxData(u8 temp,u8 length);
void TxData(u8 *rc_txbuf,u8 length);


#endif
