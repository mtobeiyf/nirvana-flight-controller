#ifndef __MYIO_H
#define __MYIO_H

#include "stm32f4xx.h"

enum Level
{
    Low = 0,
    High
};

//电磁铁驱动
void EMPlug_Init(void);
void EMPlug_Set(u8 level);

//蜂鸣器驱动 TIM8-CH3
void Buzzer_Init(void);
void Buzzer_Set(u8 level);

#endif
