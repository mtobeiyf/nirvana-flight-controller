#ifndef _USART_H
#define _USART_H

#include "stm32f4xx.h"

extern u8 Rx_Buf[];
void Usart2_Init(u32 br_num);
void Usart2_IRQ(void);
void Usart2_Send(unsigned char *DataToSend, u8 data_num);

void Uart5_Init(u32 br_num);
void Uart5_IRQ(void);
void Uart5_Send(unsigned char *DataToSend, u8 data_num);

//自定义串口
void Usart1_Init(u32 br_num);
void Usart1_IRQ(void);
void Usart1_sendbyte(u8 data);

void Usart3_Init(u32 br_num);
void Usart3_IRQ(void);
void Usart3_sendbyte(u8 data);

void FC_Data_Transfer(void);  //数据回传
void Cam_Data_Transfer(void); //摄像头数据回传
void RC_Rx_Anl(u8 *data_buf); //遥控数据解析

#endif
