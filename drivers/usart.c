/******************** (C) COPYRIGHT 2014 ANO Tech ********************************
  * 作者   ：匿名科创
 * 文件名  ：usart.c
 * 描述    ：串口驱动
 * 官网    ：www.anotc.com
 * 淘宝    ：anotc.taobao.com
 * 技术Q群 ：190169595
**********************************************************************************/

#include "usart.h"
#include "data_transfer.h"
#include "ultrasonic.h"
#include "ctrl.h"
#include "imu.h"
#include "center_ctrl.h"
#include "center_task.h"
#include "battery.h"
#include "protocol.h"
#include "height_ctrl.h"

void Usart2_Init(u32 br_num)
{
	USART_InitTypeDef USART_InitStructure;
	USART_ClockInitTypeDef USART_ClockInitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); //开启USART2时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	//串口中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);

	//配置PD5作为USART2　Tx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	//配置PD6作为USART2　Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	//配置USART2
	//中断被屏蔽了
	USART_InitStructure.USART_BaudRate = br_num;									//波特率可以通过地面站配置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//8位数据
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//在帧结尾传输1个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//禁用奇偶校验
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //硬件流控制失能
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;					//发送、接收使能
	//配置USART2时钟
	USART_ClockInitStruct.USART_Clock = USART_Clock_Disable;	 //时钟低电平活动
	USART_ClockInitStruct.USART_CPOL = USART_CPOL_Low;			 //SLCK引脚上时钟输出的极性->低电平
	USART_ClockInitStruct.USART_CPHA = USART_CPHA_2Edge;		 //时钟第二个边沿进行数据捕获
	USART_ClockInitStruct.USART_LastBit = USART_LastBit_Disable; //最后一位数据的时钟脉冲不从SCLK输出

	USART_Init(USART2, &USART_InitStructure);
	USART_ClockInit(USART2, &USART_ClockInitStruct);

	//使能USART2接收中断
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	//使能USART2
	USART_Cmd(USART2, ENABLE);
	//	//使能发送（进入移位）中断
	//	if(!(USART2->CR1 & USART_CR1_TXEIE))
	//	{
	//		USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
	//	}
}

u8 TxBuffer[256];
u8 TxCounter = 0;
u8 count = 0;

u8 Rx_Buf[256]; //串口接收缓存

void Usart2_IRQ(void)
{
	u8 com_data;

	if (USART2->SR & USART_SR_ORE) //ORE中断
	{
		com_data = USART2->DR;
	}

	//接收中断
	if (USART_GetITStatus(USART2, USART_IT_RXNE))
	{
		USART_ClearITPendingBit(USART2, USART_IT_RXNE); //清除中断标志

		com_data = USART2->DR;
#ifdef ANO_DT_USE_USART2
		ANO_DT_Data_Receive_Prepare(com_data);
#else
		OF_Data_Receive_Prepare(com_data);
#endif
	}
	//发送（进入移位）中断
	if (USART_GetITStatus(USART2, USART_IT_TXE))
	{

		USART2->DR = TxBuffer[TxCounter++]; //写DR清除中断标志
		if (TxCounter == count)
		{
			USART2->CR1 &= ~USART_CR1_TXEIE; //关闭TXE（发送中断）中断
		}

		//USART_ClearITPendingBit(USART2,USART_IT_TXE);
	}
}

void Usart2_Send(unsigned char *DataToSend, u8 data_num)
{
	u8 i;
	for (i = 0; i < data_num; i++)
	{
		TxBuffer[count++] = *(DataToSend + i);
	}

	if (!(USART2->CR1 & USART_CR1_TXEIE))
	{
		USART_ITConfig(USART2, USART_IT_TXE, ENABLE); //打开发送中断
	}
}

void Uart5_Init(u32 br_num)
{
	USART_InitTypeDef USART_InitStructure;
	//USART_ClockInitTypeDef USART_ClockInitStruct;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE); //开启USART2时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	//串口中断优先级
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_UART5);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5);

	//配置PC12作为UART5　Tx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	//配置PD2作为UART5　Rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	//配置UART5
	//中断被屏蔽了
	USART_InitStructure.USART_BaudRate = br_num;									//波特率可以通过地面站配置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//8位数据
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//在帧结尾传输1个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//禁用奇偶校验
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //硬件流控制失能
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;					//发送、接收使能
	USART_Init(UART5, &USART_InitStructure);

	//使能UART5接收中断
	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);
	//使能USART5
	USART_Cmd(UART5, ENABLE);
	//	//使能发送（进入移位）中断
	//	if(!(USART2->CR1 & USART_CR1_TXEIE))
	//	{
	//		USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
	//	}
}
u8 Tx5Buffer[256];
u8 Tx5Counter = 0;
u8 count5 = 0;

void Uart5_IRQ(void)
{
	u8 com_data;

	//接收中断
	if (USART_GetITStatus(UART5, USART_IT_RXNE))
	{
		USART_ClearITPendingBit(UART5, USART_IT_RXNE); //清除中断标志

		com_data = UART5->DR;

		Ultra_Get(com_data);
	}

	//发送（进入移位）中断
	if (USART_GetITStatus(UART5, USART_IT_TXE))
	{

		UART5->DR = Tx5Buffer[Tx5Counter++]; //写DR清除中断标志

		if (Tx5Counter == count5)
		{
			UART5->CR1 &= ~USART_CR1_TXEIE; //关闭TXE（发送中断）中断
		}

		//USART_ClearITPendingBit(USART2,USART_IT_TXE);
	}
}

void Uart5_Send(unsigned char *DataToSend, u8 data_num)
{
	u8 i;
	for (i = 0; i < data_num; i++)
	{
		Tx5Buffer[count5++] = *(DataToSend + i);
	}

	if (!(UART5->CR1 & USART_CR1_TXEIE))
	{
		USART_ITConfig(UART5, USART_IT_TXE, ENABLE); //打开发送中断
	}
}

//自定义串口驱动
void Usart1_Init(u32 br_num)
{
	GPIO_InitTypeDef GPIO_InitStruct;	// this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct;  // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			 // the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		 // this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			 // this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			 // this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOA, &GPIO_InitStruct);					 // now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = br_num;									 // the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;					 // we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;							 // we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;							 // we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;				 // we want to enable the transmitter and the receiver
	USART_Init(USART1, &USART_InitStruct);										 // again all the properties are passed to the USART_Init function which takes care of all the bit setting

	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		  // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; // this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		  // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							  // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART1, ENABLE);
}

void Camera_Rx_Anl(u8 *data_buf)
{
	camera_mode = *(data_buf + 0);
	camera_offset_x = *(data_buf + 1);
	camera_offset_y = *(data_buf + 2);
	Camera_Location_Update(); //更新位置计算
	FC_Data_Transfer();		  //立即返回数据
}

/** 接收到的字符串 */
u8 cam_rx_string[32];

//用摄像头输入
#define CAMERA_MODE
#define CAMERA_RX_LEN 4

void Usart1_IRQ(void)
{
	u8 t;
#ifdef CAMERA_MODE
	static u8 cam_cnt = 0;
	static u8 cam_rx_state = 0; // 0-等待 1-接收到FF
#endif
	// if (USART1->SR & USART_SR_ORE) //ORE中断
	// {
	// 	t = USART1->DR;
	// }

	//接收中断
	if (USART_GetITStatus(USART1, USART_IT_RXNE))
	{
		USART_ClearITPendingBit(USART1, USART_IT_RXNE); //清除中断标志

		t = USART1->DR;
#ifdef CAMERA_MODE
		if (cam_rx_state == 1)
		{
			cam_rx_string[cam_cnt] = t;
			cam_cnt++;
			if (cam_cnt == CAMERA_RX_LEN - 1)
			{
				Camera_Rx_Anl(cam_rx_string);
				cam_rx_state = 0;
			}
		}

		if (t == 0xff && cam_rx_state == 0)
		{
			cam_rx_state = 1;
			cam_cnt = 0;
		}
#else
		Usart1_sendbyte(t);
#endif
	}
}

void Usart1_sendbyte(u8 data)
{
	while (!(USART1->SR & 0x00000040))
		;

	USART_SendData(USART1, data);

	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
	{
	}
}

void Cam_Data_Transfer(void)
{
	u8 temp = 0;
	switch (Cur_Task)
	{
	case t_Manual_Control:
		temp = CAM_TRACK_XY;
		break;
	case t_Fly_A_To_B:
		temp = CAM_TRACK_XY;
		break;
	case t_Fly_Rectangle:
		temp = CAM_TRACK_XY;
		break;
	case t_Object_Track:
		temp = CAM_TRACK_COLOR;
		break;
	default:
		temp = CAM_TRACK_XY;
		break;
	}

	Usart1_sendbyte(0xff);
	Usart1_sendbyte(temp);
}

void Usart3_Init(u32 br_num)
{
	GPIO_InitTypeDef GPIO_InitStruct;	// this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct;  // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			  // the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		  // this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			  // this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			  // this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOB, &GPIO_InitStruct);					  // now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3); //
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = br_num;									 // the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;					 // we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;							 // we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;							 // we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;				 // we want to enable the transmitter and the receiver
	USART_Init(USART3, &USART_InitStruct);										 // again all the properties are passed to the USART_Init function which takes care of all the bit setting

	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;		  // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1; // this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		  // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							  // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART3, ENABLE);
}

void Usart3_sendbyte(u8 data)
{
	while (!(USART3->SR & 0x00000040))
		;

	USART_SendData(USART3, data);

	while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
	{
	}
}

//遥控发送数据解析
float Parse_PID(u8 rx_pid)
{
	if (rx_pid < 50)
		return rx_pid * 0.01; //0.01精度
	else if (rx_pid < 100)
		return 0.5 + (rx_pid - 50) * 0.02; //0.02精度
	else if (rx_pid < 150)
		return 0.5 + 1 + (rx_pid - 100) * 0.05; //0.05精度
	else if (rx_pid < 200)
		return 0.5 + 1 + 2.5 + (rx_pid - 150) * 0.1; //0.1精度
	else
		return 0;
}

void RC_Rx_Anl(u8 *data_buf)
{
	if (*(data_buf + NUM_mode) / 10 == 1)
	{
		switch (*(data_buf + NUM_parameter))
		{
		case 21:
			//前
			FLY_ANGLE_F = (*(data_buf + NUM_encoder1_counter) - 100.0) * 0.1;
			brake_time_F = (u16) * (data_buf + NUM_encoder2_counter) * 50;
			break;
		case 22:
			//后
			FLY_ANGLE_B = (*(data_buf + NUM_encoder1_counter) - 100.0) * 0.1;
			brake_time_B = (u16) * (data_buf + NUM_encoder2_counter) * 50;
			break;
		case 23:
			//左
			FLY_ANGLE_L = (*(data_buf + NUM_encoder1_counter) - 100.0) * 0.1;
			brake_time_L = (u16) * (data_buf + NUM_encoder2_counter) * 50;
			break;
		case 24:
			//右
			FLY_ANGLE_R = (*(data_buf + NUM_encoder1_counter) - 100.0) * 0.1;
			brake_time_R = (u16) * (data_buf + NUM_encoder2_counter) * 50;
			break;

		case 31:
			//内环PID
			pid_setup.groups.hc_height.kp = LIMIT(Parse_PID(*(data_buf + NUM_encoder1_counter)), 0, 10);
			pid_setup.groups.hc_height.ki = LIMIT(Parse_PID(*(data_buf + NUM_encoder2_counter)), 0, 10);
			pid_setup.groups.hc_height.kd = LIMIT(Parse_PID(*(data_buf + NUM_encoder3_counter)), 0, 10);
			break;
		case 32:
			//外环位置x
			pid_setup.groups.hc_sp.kp = LIMIT(Parse_PID(*(data_buf + NUM_encoder1_counter)), 0, 10);
			pid_setup.groups.hc_sp.ki = LIMIT(Parse_PID(*(data_buf + NUM_encoder2_counter)), 0, 10);
			pid_setup.groups.hc_sp.kd = LIMIT(Parse_PID(*(data_buf + NUM_encoder3_counter)), 0, 10);
			break;
		case 33:
			//超声波内环
			ultra_in_pid.kp = LIMIT(Parse_PID(*(data_buf + NUM_encoder1_counter)), 0, 10);
			ultra_in_pid.ki = LIMIT(Parse_PID(*(data_buf + NUM_encoder2_counter)), 0, 10);
			ultra_in_pid.kd = LIMIT(Parse_PID(*(data_buf + NUM_encoder3_counter)), 0, 10);
			break;
		case 34:
			//超声波外环
			ultra_pid.kp = LIMIT(Parse_PID(*(data_buf + NUM_encoder1_counter)), 0, 10);
			ultra_pid.ki = LIMIT(Parse_PID(*(data_buf + NUM_encoder2_counter)), 0, 10);
			ultra_pid.kd = LIMIT(Parse_PID(*(data_buf + NUM_encoder3_counter)), 0, 10);
			break;
		case 41:
			if (Cur_Task != t_Landing)
				ultra_hold_height = (u16) * (data_buf + NUM_encoder1_counter) * 10;
			break;
		case 42:
			break;
		case 43:
			break;
		case 44:
			break;

		default:
			break;
		}
	}
	if (*(data_buf + NUM_mode) / 10 == 0)
		Rc_Func = *(data_buf + NUM_function);
}

//u8 rc_rxbuf[32]; //遥控接收到的字符
//#define RC_RX_LEN 11 //接收的字长

//u8 rc_cnt = 0;
//u8 rc_rx_state = 0; // 0-等待 1-接收到FF

void Usart3_IRQ(void)
{
	//	u8 i;
	//	uint8_t t;

	if (USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
		RxData(USART_ReceiveData(USART3), RC_RX_LENGTH);
		//		t = USART_ReceiveData(USART3);
		//		USART_SendData(USART3, t);
		//		if (rc_rx_state == 1)
		//		{
		//			rc_rxbuf[rc_cnt] = t;
		//			rc_cnt++;
		//			if (rc_cnt == RC_RX_LEN - 1)
		//			{
		//				//错位判断
		//				for (i = 0; i < RC_RX_LEN - 1; i++)
		//				{
		//					if (rc_rxbuf[rc_cnt] == 0xff)
		//						break;
		//				}
		//				if (i == RC_RX_LEN - 1)
		//					RC_Rx_Anl(rc_rxbuf);
		//				rc_rx_state = 0;
		//			}
		//		}
		//		if (t == 0xFF && rc_rx_state == 0)
		//		{
		//			rc_rx_state = 1;
		//			rc_cnt = 0;
		//		}
	}
}

void FC_Data_Transfer(void)
{
	u8 i;

	rc_txbuf[NUM_pitch] = Pitch + 90;
	rc_txbuf[NUM_roll] = Roll + 90;
	rc_txbuf[NUM_yaw] = (Yaw + 180) * 5.0f / 9.0f;
	rc_txbuf[NUM_height] = ultra_distance / 10.0f;
	rc_txbuf[NUM_motor1] = motor[0] / 5.0f;
	rc_txbuf[NUM_motor2] = motor[1] / 5.0f;
	rc_txbuf[NUM_motor3] = motor[2] / 5.0f;
	rc_txbuf[NUM_motor4] = motor[3] / 5.0f;
	rc_txbuf[NUM_battery] = BatteryValue * 10.0f;

	rc_txbuf[NUM_plane_l_x] = Rc_Pwm_In[3] - 1100 / 4.0f;
	rc_txbuf[NUM_plane_l_y] = Rc_Pwm_In[2] - 1100 / 4.0f;
	rc_txbuf[NUM_plane_r_x] = except_A.x + 100;
	rc_txbuf[NUM_plane_r_y] = except_A.y + 100;

	rc_txbuf[NUM_camera_mode] = camera_mode;  //摄像头模式
	rc_txbuf[NUM_camera_x] = camera_offset_x; //x偏移
	rc_txbuf[NUM_camera_y] = camera_offset_y; //y偏移

	TxData(rc_txbuf, RC_TX_LENGTH);
	for (i = 0; i < RC_TX_LENGTH; i++)
	{
		Usart3_sendbyte(rc_txbuf[i]);
	}
	/* 等待发送完成 */
	while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
		;

	//	u8 temp = 0;
	//	Usart3_sendbyte(0xFF); // 字头

	//	temp = Pitch + 90;
	//	Usart3_sendbyte(temp); // P俯仰
	//	temp = Roll + 90;
	//	Usart3_sendbyte(temp); // R横滚

	//	temp = (Yaw + 180) * 5.0f / 9.0f;
	//	Usart3_sendbyte(temp); // Y航偏

	//	temp = ultra_distance / 10.0f;
	//	Usart3_sendbyte(temp); // 高度

	//	temp = motor[0] / 5.0f;
	//	Usart3_sendbyte(temp); // 油门1
	//	temp = motor[1] / 5.0f;
	//	Usart3_sendbyte(temp); // 油门2
	//	temp = motor[2] / 5.0f;
	//	Usart3_sendbyte(temp); // 油门3
	//	temp = motor[3] / 5.0f;
	//	Usart3_sendbyte(temp); // 油门4
	//	temp = BatteryValue * 10.0f;
	//	Usart3_sendbyte(temp); // 电池

	//	temp = Rc_Pwm_In[3] - 1100 / 4.0f;
	//	Usart3_sendbyte(temp); // lx
	//	temp = Rc_Pwm_In[2] - 1100 / 4.0f;
	//	Usart3_sendbyte(temp); // ly
	//	// temp = (u8)LIMIT(((double)LIMIT((Rc_Pwm_In[0] - 1100), 0, 800) / 4.0f), 0, 200);
	//	// Usart3_sendbyte(temp); // rx
	//	// temp = (u8)LIMIT(((double)LIMIT((Rc_Pwm_In[1] - 1100), 0, 800) / 4.0f), 0, 200);
	//	// Usart3_sendbyte(temp); // ry
	//	temp = except_A.x + 100;
	//	Usart3_sendbyte(temp);
	//	temp = except_A.y + 100;
	//	Usart3_sendbyte(temp);

	//	Usart3_sendbyte(camera_mode);	 //摄像头模式
	//	Usart3_sendbyte(camera_offset_x); //x偏移
	//	Usart3_sendbyte(camera_offset_y); //y偏移

	//TO ADD
}

/******************* (C) COPYRIGHT 2014 ANO TECH *****END OF FILE************/
